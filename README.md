# Link for Website NFT project

https://sofiane-wattiez.github.io/WEBSITE-FOR-NFT/

New d'apps for minting coming soon for one other project

# OpenSea Collection

https://opensea.io/collection/funnybears

# Banner

![image](https://user-images.githubusercontent.com/71489111/145406973-b41903ef-a128-45fb-8a0c-443aa56e0768.png)

# Keyframe animation

![image](https://user-images.githubusercontent.com/71489111/145407537-00ed2d7c-903a-482f-8233-172d1188ec37.png)

# Road Map

![image](https://user-images.githubusercontent.com/71489111/145410167-ef3ddc1e-14f9-40a8-b510-ac9cd48b3219.png)


# Responsive Version

I need completed responsive version
